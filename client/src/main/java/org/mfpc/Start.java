package org.mfpc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.mfpc.controller.IController;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Start extends Application {

    public static void main(String[] args) {

//        Registry registry = null;
//        try {
//            registry = LocateRegistry.getRegistry();
//            IController server = (IController) registry.lookup("BankService");
//
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        } catch (NotBoundException e) {
//            e.printStackTrace();
//        }

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sample.fxml"));
        primaryStage.setTitle("Bank App");
        primaryStage.setScene(new Scene(root, 1000, 400));
        primaryStage.show();
    }
}
