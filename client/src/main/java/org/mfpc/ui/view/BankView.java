package org.mfpc.ui.view;

import org.mfpc.domain.Bank;

public class BankView {

    private Integer id;
    private String name;
    private String address;

    public BankView(Bank bank) {
        this.name = bank.getName();
        this.address = bank.getAddress();
        this.id = bank.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name;
    }
}
