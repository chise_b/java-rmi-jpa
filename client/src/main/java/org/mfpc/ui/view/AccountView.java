package org.mfpc.ui.view;

import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.domain.Person;

public class AccountView {

    private Integer id;

    private Double sum;

    private Bank bank;

    private Person person;

    public AccountView(Account account) {
        this.id = account.getId();
        this.sum = account.getSum();
        this.bank = account.getBank();
        this.person = account.getPerson();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        if (person != null){
            return person.getName();
        }
        return "undefined name";
    }
}
