package org.mfpc.ui;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.mfpc.controller.IController;
import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.domain.Person;
import org.mfpc.ui.view.AccountView;
import org.mfpc.ui.view.BankView;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;


public class UIController {

    private IController server;

    @FXML
    private TextField textFieldBankName;

    @FXML
    private TextField textFieldBankAddress;

    @FXML
    private Button buttonCreateBank;

    @FXML
    private ListView listViewBank;

    @FXML
    private TextField textFieldNameAccount;

    @FXML
    private TextField textFieldAgeAccount;

    @FXML
    private TextField textFieldCNPAccount;

    @FXML
    private ListView listViewAccounts;

    @FXML
    private TextField textFieldCurrentSum;

    @FXML
    private TextField textFieldChangeSum;

    private ObservableList<BankView> banksObservable = FXCollections.observableArrayList();
    private ObservableList<AccountView> accountsObservable = FXCollections.observableArrayList();


    @FXML
    public void createBank(){
        String bankName = textFieldBankName.getText();
        String bankAddresss = textFieldBankAddress.getText();

        Bank bank = new Bank(bankName, bankAddresss);

        try {
            server.createBank(bank);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        clearBank();

        initializeBanks();
    }

    private void initializeBanks(){
        try {
            List<Bank> banks = server.viewAllBanks();

            final List<BankView> banksView = new ArrayList<BankView>();
            banks.forEach(bank -> banksView.add(new BankView(bank)));

            banksObservable.clear();
            banksObservable.addAll(banksView);

            listViewBank.setItems(banksObservable);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void initializeAccounts(){
        try {
            BankView bankView = (BankView) listViewBank.getSelectionModel().getSelectedItem();
            List<Account> accounts = server.viewAllPersonsFromABank(bankView.getId());

            final List<AccountView> accountViews = new ArrayList<>();
            accounts.forEach(account -> accountViews.add(new AccountView(account)));

            accountsObservable.clear();
            accountsObservable.addAll(accountViews);

            listViewAccounts.setItems(accountsObservable);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void initialize(){
        Registry registry = null;
        try {
            registry = LocateRegistry.getRegistry();
            server = (IController) registry.lookup("BankService");
            initializeBanks();

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void selectBank(){
        listViewBank.setOnMouseClicked(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                BankView bankView = (BankView) listViewBank.getSelectionModel().getSelectedItem();
                textFieldBankName.setText(bankView.getName());
                textFieldBankAddress.setText(bankView.getAddress());

                initializeAccounts();

                textFieldCurrentSum.clear();
                clearAccount();
            }
        });
    }

    @FXML
    public void selectAccount(){
        listViewAccounts.setOnMouseClicked(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                AccountView accountView = (AccountView) listViewAccounts.getSelectionModel().getSelectedItem();
                textFieldNameAccount.setText(accountView.getPerson().getName());
                textFieldAgeAccount.setText(accountView.getPerson().getAge().toString());
                textFieldCNPAccount.setText(accountView.getPerson().getCNP());
                textFieldCurrentSum.setText(accountView.getSum().toString());
            }
        });
    }

    @FXML
    public void deleteBank(){
        BankView bankView = (BankView) listViewBank.getSelectionModel().getSelectedItem();
        Bank bank = new Bank(bankView.getId(), bankView.getName(), bankView.getAddress());
        try {
            server.deleteBank(bank);
            initializeBanks();

            accountsObservable.clear();

            clearBank();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void updateBank(){
        BankView bankView = (BankView) listViewBank.getSelectionModel().getSelectedItem();
        Bank bank = new Bank(bankView.getId(), textFieldBankName.getText(), textFieldBankAddress.getText());
        try {
            server.editBanK(bank);
            initializeBanks();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void clearBank(){
        textFieldBankName.clear();
        textFieldBankAddress.clear();
    }

    @FXML
    public void clearAccount(){
        textFieldNameAccount.clear();
        textFieldAgeAccount.clear();
        textFieldCNPAccount.clear();
    }

    @FXML
    public void addMoney(){
        AccountView accountView = (AccountView) listViewAccounts.getSelectionModel().getSelectedItem();
        Double amountToAdd = Double.parseDouble(textFieldChangeSum.getText());

        try {
            Account newAccount = server.addMoney(accountView.getId(), amountToAdd);
            textFieldCurrentSum.setText(newAccount.getSum().toString());
            textFieldChangeSum.clear();
            initializeAccounts();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void withdrawMoney(){
        AccountView accountView = (AccountView) listViewAccounts.getSelectionModel().getSelectedItem();
        Double amountToAdd = Double.parseDouble(textFieldChangeSum.getText());

        try {
            Account newAccount = server.withdrawMoney(accountView.getId(), amountToAdd);
            textFieldCurrentSum.setText(newAccount.getSum().toString());
            textFieldChangeSum.clear();
            initializeAccounts();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void createAccount(){
        BankView bankView = (BankView) listViewBank.getSelectionModel().getSelectedItem();

        String personName = textFieldNameAccount.getText();
        String personCNP = textFieldCNPAccount.getText();
        Integer personAge = Integer.parseInt(textFieldAgeAccount.getText()) ;

        Person person = new Person(personCNP, personName, personAge);

        try {
            server.createAccount(person, bankView.getId());
            clearAccount();
            initializeAccounts();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void deleteAccount(){
        AccountView accountView = (AccountView) listViewAccounts.getSelectionModel().getSelectedItem();
        try {
            server.deleteAccount(accountView.getId());
            initializeAccounts();
            clearAccount();
            textFieldCurrentSum.clear();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


}
