package org.mfpc.domain;

import java.io.Serializable;

public class Account implements Serializable {

    private Integer id;

    private Double sum;

    private Bank bank;

    private Person person;

    public Account() {
        sum = 0.0;
    }

    public Account(Bank bank, Person person){
        this.bank = bank;
        this.person = person;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
