package org.mfpc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Person implements Serializable {

    private Integer id;

    private String CNP;

    private String name;

    private Integer age;

    private List<Account> accounts = new ArrayList<Account>();

    public Person(String CNP, String name, Integer age) {
        this.CNP = CNP;
        this.name = name;
        this.age = age;
    }

    public Person(){}

    public void addAccount(Account account) {
        accounts.add(account);
        account.setPerson(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.setPerson(null);
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
