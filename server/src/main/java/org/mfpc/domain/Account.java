package org.mfpc.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Double sum;

    @ManyToOne(fetch = FetchType.EAGER)
    private Bank bank;

    @ManyToOne(fetch = FetchType.EAGER)
    private Person person;

    public Account(Bank bank, Person person){
        this.bank = bank;
        this.person = person;
        this.sum = 0.0;
    }

    public Account() {
        sum = 0.0;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
