package org.mfpc.controller;

import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.domain.Person;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IController extends Remote {

    Person viewPerson(String CNP) throws RemoteException;

    Bank viewBank(Integer bankId) throws RemoteException;

    void createBank(Bank bank) throws RemoteException;

    Bank editBanK(Bank bank) throws RemoteException;

    void deleteBank(Bank bank) throws RemoteException;

    Person createAccount(Person person, Integer bankId) throws RemoteException;

    List<Account> viewAllPersonsFromABank(Integer bankId) throws RemoteException;

    List<Bank> viewAllBanks() throws RemoteException;

    void deleteAccount(Integer accountId) throws RemoteException;

    Account addMoney(Integer accountId, Double amount) throws RemoteException;

    Account withdrawMoney(Integer accountId, Double amount) throws RemoteException;
}
