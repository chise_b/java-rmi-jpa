package org.mfpc.controller;

import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.domain.Person;
import org.mfpc.repository.AccountRepository;
import org.mfpc.repository.BankRepository;
import org.mfpc.repository.PersonRepository;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Controller implements IController {

    private BankRepository bankRepository;
    private AccountRepository accountRepository;
    private PersonRepository personRepository;

    public Controller(){

        bankRepository = new BankRepository();
        accountRepository = new AccountRepository();
        personRepository = new PersonRepository();
    }

    public void createStubAndBind() throws RemoteException {

        IController stub = (IController) UnicastRemoteObject.exportObject((IController) this, 0);
        Registry registry = LocateRegistry.createRegistry(1099);
        registry.rebind("BankService", stub);
        System.out.println("Service started...");
    }

    public void createBank(Bank bank) {
        bankRepository.addBank(bank);
    }

    public Bank viewBank(Integer bankId) {
        return bankRepository.findBankById(bankId);
    }

    public Person viewPerson(String CNP) {
        return null;
    }

    public Bank editBanK(Bank bank) {
        Bank toUpdateBank = bankRepository.findBankById(bank.getId());

        toUpdateBank.setAddress(bank.getAddress());
        toUpdateBank.setName(bank.getName());

        return bankRepository.updateBank(toUpdateBank);
    }

    public void deleteBank(Bank bank) {
        Bank toDeleteBank = bankRepository.findBankById(bank.getId());
        bankRepository.deleteBank(toDeleteBank);
    }

    public Person createAccount(Person person, Integer bankId) {

        Person existingPerson = personRepository.findPersonByCNP(person.getCNP());
        if (existingPerson == null){
            personRepository.addPerson(person);
        }

        Bank bank = bankRepository.findBankById(bankId);
        Person storedPerson = personRepository.findPersonByCNP(person.getCNP());
        Account account = new Account(bank, storedPerson);
        accountRepository.addAccount(account);

        storedPerson.addAccount(account);
        storedPerson = personRepository.updatePerson(storedPerson);

        return storedPerson;
    }

    public List<Account> viewAllPersonsFromABank(Integer bankId) {
        return accountRepository.getAllPersonsFromBank(bankId);
    }

    public List<Bank> viewAllBanks() {
        List<Bank> banks = bankRepository.getAllBanks();
        return banks;
    }

    public void deleteAccount(Integer accountId) {
        Account account = accountRepository.findAccountById(accountId);
        accountRepository.deleteAccount(account);
    }

    public Account addMoney(Integer accountId, Double amount) throws RemoteException {
        if (amount > 0){
            Account account = accountRepository.findAccountById(accountId);
            account.setSum(account.getSum() + amount);
            return accountRepository.updateAccount(account);
        }
        return accountRepository.findAccountById(accountId);

    }

    public Account withdrawMoney(Integer accountId, Double amount) throws RemoteException {
        Account account = accountRepository.findAccountById(accountId);
        if (amount <= account.getSum() && amount > 0){
            account.setSum(account.getSum() - amount);
            return accountRepository.updateAccount(account);
        }
        return account;
    }
}
