package org.mfpc.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
    private static SessionFactory sessionFactory = null;
    private static StandardServiceRegistry serviceRegistry = null;
    private static final String CONFIGURATION = "hibernate.cfg.xml";

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null){
            serviceRegistry = new StandardServiceRegistryBuilder().configure(CONFIGURATION).build();
            Metadata meta = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
            sessionFactory = meta.getSessionFactoryBuilder().build();
        }
        return sessionFactory;
    }

    public static void closeFactory(){
        sessionFactory.close();
    }
}

