package org.mfpc.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.util.HibernateUtil;

import java.util.List;

public class AccountRepository {

    public void addAccount(Account account) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.persist(account);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Account> getAllPersonsFromBank(Integer id) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<Account> accountsFromBank = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Account where bank.id = :id");
            query.setInteger("id", id);
            accountsFromBank = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return accountsFromBank;
    }

    public Account findAccountById(Integer id){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        Account account = null;

        try {
            tx = session.beginTransaction();
            account = (Account) session.get(Account.class, id);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return account;
    }

    public void deleteAccount(Account account){

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(account);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Account updateAccount(Account oldAccount){

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        Account newAccount = null;

        try {
            tx = session.beginTransaction();
            newAccount = (Account) session.merge(oldAccount);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return newAccount;
    }

}
