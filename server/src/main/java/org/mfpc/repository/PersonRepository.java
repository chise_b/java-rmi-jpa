package org.mfpc.repository;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.mfpc.domain.Account;
import org.mfpc.domain.Bank;
import org.mfpc.domain.Person;
import org.mfpc.util.HibernateUtil;

import java.util.List;

public class PersonRepository {

    public List<Account> getAllAccountsFromBank(Integer bankId) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List<Account> accountsFromBank = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Account where bank.id = :id");
            query.setInteger("id", bankId);
            accountsFromBank = (List<Account>) query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return accountsFromBank;
    }

    public void addPerson(Person person){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.persist(person);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Person findPersonByCNP(String CNP){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        Person person = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Person where CNP = :CNP");
            query.setString("CNP", CNP);
            person = (Person) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return person;
    }

    public Bank findPersonById(Integer id){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        Bank bank = null;

        try {
            tx = session.beginTransaction();
            bank = (Bank) session.get(Bank.class, id);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return bank;
    }

    public Person updatePerson(Person oldPerson){

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        Person newPerson = null;

        try {
            tx = session.beginTransaction();
            newPerson = (Person) session.merge(oldPerson);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return newPerson;
    }
}
