package org.mfpc;

import org.hibernate.SessionFactory;
import org.mfpc.controller.Controller;
import org.mfpc.domain.Bank;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.mfpc.util.HibernateUtil;

import java.rmi.RemoteException;
import java.util.List;

public class Start {

    public static void main(String[] args) {
        try {
            Controller server = new Controller();
            server.createStubAndBind();
        } catch (RemoteException e) {
            System.out.println(e.getMessage());
        }
    }

//    public static void main(String[] args) {
//        Controller controller = new Controller();
//        Bank bank = controller.viewBank(2);
//        controller.deleteBank(bank);
//    }
}
